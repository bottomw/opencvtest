// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <string>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/opencv.hpp"
#include <cvaux.h>

//#include "vld.h"
#include <afxwin.h>
#include <opencv\cv.h>

#include <vector>
#include <algorithm>
#include <iterator>
#include <list>
#include <queue>
#include <sstream>

#include "tinyxml.h"
#include "tinystr.h"

#define defaultWindowWidth 513
//#define mainWindowHeight 860

#define halfIconHeight 300

#define defaultClickPointX 267
#define defaultClickPointY 433
#define intervalMS 500

static double light(IplImage *image)
{  
    IplImage * gray = cvCreateImage(cvGetSize(image), image->depth, 1);  
    cvCvtColor(image, gray, CV_BGR2GRAY);    
    double sum = 0;    
    double avg = 0;  
    CvScalar scalar;  
    int ls[256];  
    for(int i=0; i<256; i++)  
        ls[i]=0;  
    for(int i=0;i<gray->height;i++)    
    {    
        for(int j=0;j<gray->width;j++)    
        {    
            scalar = cvGet2D(gray, i, j);  
            sum += (scalar.val[0] - 128);  
            int x= (int)scalar.val[0];    
             ls[x]++;    
        }    
    }    
    avg = sum/(gray->height * gray->width);    
    double total = 0;  
    double mean = 0;  
    for(int i=0;i<256;i++)    
    {    
        total += abs(i-128-avg)* ls[i];    
    }    
    mean = total/(gray->height * gray->width);
	return mean;
    //double cast = abs(avg/mean);    
    //printf("light: %f\n", cast);  
    //if(cast>1)  
    //{  
    //    if(avg>0)  
    //        printf("light\n");  
    //    else printf("dark\n");  
    //}  
}  

static CString GetAppPath()
{
	//获取应用程序根目录
    TCHAR modulePath[MAX_PATH];
    GetModuleFileName(NULL, modulePath, MAX_PATH);
    CString strModulePath(modulePath);
    strModulePath = strModulePath.Left(strModulePath.ReverseFind(_T('\\')));
    return strModulePath;
}