#include "stdafx.h"
#include "LocatePosition.h"


LocatePoistion::LocatePoistion(void)
{
}


LocatePoistion::~LocatePoistion(void)
{
}

void LocatePoistion::source_image(IplImage *srcImage)
{
	this->sourceImage = srcImage;
}

void LocatePoistion::locate_image_subimage(IplImage *anchorImage, CvPoint *minLoc, CvPoint *maxLoc)
{
	IplImage *result;
	
	double minVal;  
    double maxVal;  

	int iwidth = sourceImage->width - anchorImage->width + 1;  
    int iheight = sourceImage->height - anchorImage->height + 1;

	result = cvCreateImage(cvSize(iwidth, iheight), 32, 1);
	cvMatchTemplate(sourceImage, anchorImage, result, 0);
	cvMinMaxLoc(result, &minVal, &maxVal, minLoc, maxLoc, NULL);

	cvReleaseImage(&result);
}


/*
void LocatePoistion::locate_image_light(CvPoint minLoc, CvPoint maxLoc)
{
	Vector<Point> subImagePositionList;
	Vector<double> subImageLightList;

	int cnt = 0;

	CvSize mainWindowSize = cvSize(mainWindowWidth, mainWindowHeight);

	for(int widthCursor = 0; widthCursor <= mainWindowWidth; widthCursor += subImageSize + 1)
	{
		for(int heightCursor = 0; heightCursor <= mainWindowHeight; heightCursor += subImageSize + 1)
		{
			int widthCursorEnd = min(widthCursor + subImageSize, mainWindowWidth);
			int heightCursorEnd = min(heightCursor + subImageSize, mainWindowWidth);
			//Rect rect(widthCursor, heightCursor, widthCursorEnd, heightCursorEnd);

			IplImage *tmpWindow = cvCreateImage(mainWindowSize, sourceImage->depth, sourceImage->nChannels);
			cvCopy(sourceImage, tmpWindow);

			CvRect box = cvRect(widthCursor, heightCursor, subImageSize, subImageSize);
			cvSetImageROI(tmpWindow, box);

			//cvShowImage(window_name, tmpWindow);
			subImagePositionList.push_back(Point(widthCursor, heightCursor));
			double lightVal = light(tmpWindow);
			subImageLightList.push_back(lightVal);
			cvReleaseImage(&tmpWindow);
			//cout<<"width:"<<widthCursor<<"height:"<<heightCursor<<'\n';
		}
	};

	
	double maxLight = 0.0;
	for(int i = 0; i != subImageLightList.size(); i++)
	{
		maxLight = subImageLightList[i] > maxLight ? subImageLightList[i] : maxLight;
		//cout<<"坐标"<<subImagePositionList[i]<<"的亮度为："<<subImageLightList[i]<<'\n';
	}

	for(int i = 0; i != subImageLightList.size(); i++)
	{
		if(subImageLightList[i] == maxLight)
		{
			cout<<"亮度最高"<<subImagePositionList[i]<<"，坐标为："<<subImagePositionList[i]<<'\n';

			IplImage *tmpWindow = cvCreateImage(mainWindowSize, sourceImage->depth, sourceImage->nChannels);
			cvCopy(sourceImage, tmpWindow);

			CvRect box = cvRect(subImagePositionList[i].x, subImagePositionList[i].y, subImageSize, subImageSize);
			cvSetImageROI(tmpWindow, box);
			cvShowImage(sub_window_name, tmpWindow);
			minLoc.x = subImagePositionList[i].x;
			minLoc.y = subImagePositionList[i].y;
			maxLoc.x = subImagePositionList[i].x + subImageSize;
			maxLoc.y = subImagePositionList[i].y + subImageSize;
			cvReleaseImage(&tmpWindow);
		}
	}
}
*/