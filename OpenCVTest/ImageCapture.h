#pragma once
#include "stdafx.h"
#include "LocatePosition.h"

using namespace cv;
using namespace std;

//static Mat g_img_src;
//static Mat g_img_dst;
//static Mat g_img_sub;
//static bool g_is_rect_inited = false;
//static Point g_rect_tl;
static const char* main_window_name = "main window";

class ImageCapture
{
public:
	static ImageCapture *get_instance();
	~ImageCapture(void);

	HBITMAP hBitmap;
	HDC hDDC;
	HDC hCDC;

	LPVOID screenCaptureData;

	int nWidth;
	int nHeight;

	//int mainWindowWidth;
	//int mainWindowHeight;

	IplImage *img;
	IplImage *img2;
	IplImage *mainWindow;
	IplImage *anchorImage;
	IplImage *anchorImage2;

	CvPoint minLoc;  
	CvPoint maxLoc;

	CvPoint minLoc2;  
	CvPoint maxLoc2;

	int anchorHeight;

	CvSize mainWindowSize;

	LocatePoistion locatePosition;

	void init_gdi();
	void get_anchor();
	Point gdi_screen_capture();
	void release_image();
	bool anchorFlag;
private:
	ImageCapture(void);
	static ImageCapture *shareImageCapture;
};

