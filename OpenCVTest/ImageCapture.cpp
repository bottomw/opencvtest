#include "stdafx.h"
#include "ImageCapture.h"



ImageCapture::ImageCapture(void)
{
	anchorFlag = false;
}


ImageCapture::~ImageCapture(void)
{
}

ImageCapture *ImageCapture::shareImageCapture = nullptr;
ImageCapture *ImageCapture::get_instance()
{
	if(shareImageCapture == NULL)
	{

		shareImageCapture = new ImageCapture();
		return shareImageCapture;
	}
}

void ImageCapture::init_gdi()
{
	nWidth = GetSystemMetrics(SM_CXSCREEN);
	nHeight = GetSystemMetrics(SM_CYSCREEN);
	screenCaptureData = new char[nWidth*nHeight*4];
	memset(screenCaptureData, 0, nWidth);
	
	hDDC = GetDC(GetDesktopWindow());  //DC全称Device Context设备描述表，C++中在所有类前加字母C，即CDC,得到该函数检索一指定窗口的客户区域或整个屏幕的显示设备上下文环境的句柄，以后可以在GDI函数中使用该句柄来在设备上下文环境中绘图。
	//标记桌面窗口DC
	//设备上下文是一种包含有关某个设备（如显示器或打印机）的绘制属性信息的 Windows 数据结构。
	hCDC = CreateCompatibleDC(hDDC);  //该函数创建一个与指定设备兼容的内存设备上下文环境（DC）。通过GetDc()获取的HDC直接与相关设备沟通
	//建立与桌面窗口兼容的内存缓冲区,目标区域
	hBitmap = CreateCompatibleBitmap(hDDC, nWidth, nHeight);//返回位图地址
	SelectObject(hCDC, hBitmap);//把一个对象(位图、画笔、画刷等)选入指定的设备描述表。新的对象代替同一类型的老对象。
}

void ImageCapture::get_anchor()
{
	if(!anchorFlag)
	{
		this->anchorImage = cvLoadImage("anchor.jpg", CV_BGRA2BGR);
		this->anchorImage2 = cvLoadImage("anchor2.jpg", CV_BGRA2BGR);
		this->anchorHeight = anchorImage->height;
	}
}

Point ImageCapture::gdi_screen_capture()
{
	BitBlt(hCDC, 0, 0, nWidth, nHeight, hDDC, 0, 0, SRCCOPY);//该函数对指定的源设备环境区域中的像素进行位块（bit_block）转换，以传送到目标设备环境。
	//实际在hCDC中写入数据并被hBitmap所标记
	GetBitmapBits(hBitmap, nWidth*nHeight*4, screenCaptureData);//从hBitmap中获得数据并存到screenCaptureData中
	img = cvCreateImage(cvSize(nWidth, nHeight), 8, 4); //创建一个rgba格式的IplImage,图像结构体，将矩形解释为图像
	memcpy(img->imageData, screenCaptureData, nWidth*nHeight*4);
	img2 = cvCreateImage(cvSize(nWidth, nHeight), 8, 3);
	cvCvtColor(img, img2, CV_BGRA2BGR); //Opencv里的颜色空间转换函数，可以实现BGR颜色向HSV，HSI等颜色空间的转换，也可以转换为灰度图像。

	if(!anchorFlag)
	{
		locatePosition.source_image(img2);
		locatePosition.locate_image_subimage(anchorImage, &minLoc, &maxLoc);
		locatePosition.locate_image_subimage(anchorImage2, &minLoc2, &maxLoc2);
		anchorFlag = true;
	}
	//cvRectangle(img2, cvPoint(minLoc.x - 5, minLoc.y ), cvPoint((minLoc.x + mainWindowWidth), (minLoc.y + mainWindowHeight)), CV_RGB(0,255,0), 1);

	CvSize mainWindowSize;
	mainWindowSize.width = minLoc2.x - minLoc.x;
	mainWindowSize.height = ((maxLoc2.y - (minLoc.y + this->anchorHeight)) + halfIconHeight) * 2;

	CvSize mainSize;
	mainSize.width = min(mainWindowSize.width, nWidth - minLoc.x);
	mainSize.height = min(mainWindowSize.height, nHeight - minLoc.y);
	CvRect box = cvRect(minLoc.x, minLoc.y, mainSize.width, mainSize.height);
	mainWindow = cvCreateImage(mainSize, img2->depth, img2->nChannels);
	cvSetImageROI(img2, box);
	cvCopy(img2, mainWindow);

	//namedWindow(main_window_name, CV_WINDOW_AUTOSIZE);
	//cvShowImage(main_window_name, mainWindow);
	Point basicPoint = Point(minLoc.x, minLoc.y);
	return basicPoint;
}

void ImageCapture::release_image()
{
	cout<<"release memeory"<<endl;
	::DeleteObject(hBitmap);
	::DeleteObject(hDDC);
	::DeleteObject(hCDC);

	cvReleaseImage(&img);
	cvReleaseImage(&img2);
	cvReleaseImage(&mainWindow);
	anchorImage = NULL;
	delete[] screenCaptureData;
	//cvReleaseImage(&anchorImage);
}
 
