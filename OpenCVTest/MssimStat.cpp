#include "stdafx.h"
#include "MssimStat.h"


MssimStat::MssimStat(void)
{
}


MssimStat::~MssimStat(void)
{
}

bool MssimStat::compareFlag = false;
MssimStat *MssimStat::shareMssimStat = nullptr;
MssimStat *MssimStat::get_instance()
{
	if(shareMssimStat == NULL)
	{
		shareMssimStat = new MssimStat();

		return shareMssimStat;
	}
}

/*
void MssimStat::queue_image(IplImage* img)
{
	CvSize windowSize;
	windowSize.height = img->height;
	windowSize.width = img->width;
	IplImage* mainWindowCpy = cvCreateImage(windowSize, img->depth, img->nChannels);
	cvCopy(img, mainWindowCpy);
	if(imageQueue.size() >= 2)
	{

		imageQueue.pop();
	}
	else if(imageQueue.size() == 0)
	{
		imageQueue.push(mainWindowCpy);
	}
	else if(imageQueue.size() == 1)
	{
		MssimStat::compareFlag = true;
	}
	imageQueue.push(mainWindowCpy);
}
*/

void MssimStat::queue_image(IplImage* img)
{
	CvSize windowSize;
	windowSize.height = img->height;
	windowSize.width = img->width;
	Mat mainWindowCpy = Mat(img, true);
	if(imageQueue.size() >= 2)
	{
		imageQueue.pop();
	}
	else if(imageQueue.size() == 0)
	{
		imageQueue.push(mainWindowCpy);
	}
	else if(imageQueue.size() == 1)
	{
		MssimStat::compareFlag = true;
	}
	imageQueue.push(mainWindowCpy);
}

double MssimStat::getPSNR()
{
	if(!MssimStat::compareFlag)
	{cout<<"首帧"<<endl;}
	Mat diffImage;
	Mat formalMat = imageQueue.front();
	Mat newMat = imageQueue.back();
	absdiff(formalMat, newMat, diffImage);
	diffImage.convertTo(diffImage, CV_32F);//转变为八位
	diffImage = diffImage.mul(diffImage); //diffImage ^ 2

	CvScalar s = sum(diffImage); //sum element by channel
	double sse = s.val[0] + s.val[1] + s.val[2]; //sum channels
	if(sse <= 1e-10)
		return 0;
	else
	{
		double mse = sse/(double)(formalMat.channels() * newMat.total());
		double psnr = 10.0*log10((255*255)/mse);
		return psnr;
	}
}

bool MssimStat::scene_tranfer()
{
	CvScalar mssim = this->getMSSIM();

	bool sceneTransFlag = false;
	for(int channel = 0; channel <=2; channel++)
	{
		//cout<<"mssim value: B-"<<mssim.val[0]<<" G-"<<mssim.val[1]<<" R-"<<mssim.val[2]<<endl;
		if(mssim.val[channel] < 0.8)
		{
			cout<<"mssim value: B-"<<mssim.val[0]<<" G-"<<mssim.val[1]<<" R-"<<mssim.val[2]<<endl;
			sceneTransFlag = true;
			break;
		}
	}
	return sceneTransFlag;
}

CvScalar MssimStat::getMSSIM()
{
	if(!MssimStat::compareFlag)
	{cout<<"首帧"<<endl;}
	const double C1 = 6.5025, C2 = 58.5225;
	int d = CV_32F;

	Mat formalImage, newImage;
	Mat formalMat, newMat;
	formalMat = imageQueue.front();
	newMat = imageQueue.back();
	formalMat.convertTo(formalImage, d);
	newMat.convertTo(newImage, d);

	Mat formalImage2 = formalImage.mul(formalImage);
	Mat newImage2 = newImage.mul(newImage);
	Mat new_formal = newImage.mul(formalImage);

	Mat sigma1_1, sigma2_2, sigma1_2;
	
	cout<<"check point 1"<<endl;
	GaussianBlur(new_formal, sigma1_2, Size(11, 11), 1.5);//高斯模糊
	sigma1_2 -= new_formal;

	GaussianBlur(formalImage2, sigma1_1, Size(11, 11), 1.5);//高斯模糊
	sigma1_1 -= formalImage2;

	GaussianBlur(newImage2, sigma2_2, Size(11, 11), 1.5);//高斯模糊
	sigma2_2 -= newImage2;
	cout<<"check point 2"<<endl;

	Mat t1, t2, t3;

	t1 = 2*new_formal + C1;
	t2 = 2*sigma1_2 + C2;
	t3 = t1.mul(t2);

	t1.release();
	t2.release();

	t1 = new_formal + newImage2 + C1;
	t2 = sigma1_1 + sigma2_2 + C2;
	t1 = t1.mul(t2);
	cout<<"check point 3"<<endl;

	Mat ssimMap;
	divide(t3, t1, ssimMap);
	CvScalar mssim = mean(ssimMap);
	
	t1.release();
	t2.release();
	t3.release();
	ssimMap.release();

	sigma1_1.release();
	sigma2_2.release();
	sigma1_2.release();

	cout<<"check point 4"<<endl;
	return mssim;
}