#include "stdafx.h"
#include "EpisodeConfRead.h"


EpisodeConfRead::EpisodeConfRead(void)
{
}


EpisodeConfRead::~EpisodeConfRead(void)
{
}

EpisodeConfRead* EpisodeConfRead::shareEpisodeConfRead = nullptr;

EpisodeConfRead *EpisodeConfRead::get_instance()
{
	if(shareEpisodeConfRead == NULL)
	{
		shareEpisodeConfRead = new EpisodeConfRead;
		return shareEpisodeConfRead;
	}
}

void EpisodeConfRead::init(string &confFileName)
{
	episodeCurserFlag = 0;
	CString appPathTmp = GetAppPath();
	string appPath = appPathTmp.GetBuffer(0);
	string seperator = "\\";
	string fullPath = appPath + seperator + confFileName;
	//XML文档对象
	TiXmlDocument *myDoc = new TiXmlDocument(confFileName.c_str());
	myDoc->LoadFile();
	rootElement = myDoc->RootElement();
	cout << rootElement->Value() << endl;
	episodeCurser = rootElement->FirstChildElement();
	cout <<episodeCurser->Value()<< endl;
}

bool EpisodeConfRead::curser_end()
{
	if(episodeCurser != NULL)
	{
		return false;
	}
	else
	{
		cout<<"最后一幕"<<endl;
		return true;
	}
}

void EpisodeConfRead::move_episode_curser()
{
	episodeCurserFlag++;
	episodeCurser = episodeCurser->NextSiblingElement();
	if(episodeCurser->FirstAttribute()->Value() == "guide")
	{
		cout<<"引导幕"<<endl;
	};
	cout<<"下一幕"<<endl;
}

bool EpisodeConfRead::episode_type(string episodeTypeName)
{
	episodeType = episodeCurser->FirstAttribute();
	episodeFlag = episodeType->Next();
	string episodeType = this->episodeType->Value();
	if(episodeType.compare(episodeTypeName) == 0)
	{
		return true;
	}
	return false;
}

bool EpisodeConfRead::episode_ignore()
{
	string episodeFlag = this->episodeFlag->Value();
	if(episodeFlag.compare("0") == 0)
	{
		return true;
	}
	return false;
}


void EpisodeConfRead::read_xml_curser()
{
	//cout<<episodeCurser->Value()<<endl;
	
	//cout<<episodeFeatureID->GetText()<<endl;
	if(this->episode_type("normal"))
	{
		episodeFeatureID = episodeCurser->FirstChildElement();
		TiXmlElement* featureImageOffset = episodeFeatureID->NextSiblingElement();

		imageOffsetX = featureImageOffset->FirstChildElement();
		imageOffsetY = imageOffsetX->NextSiblingElement();

		TiXmlElement* options = featureImageOffset->NextSiblingElement();
		TiXmlElement* option = options->FirstChildElement();

		TiXmlElement* optionOffset = option->FirstChildElement();
		optionOffsetX = optionOffset->FirstChildElement();
		optionOffsetY = optionOffsetX->NextSiblingElement();

	}
	else if(this->episode_type("guide"))
	{
		guideFeatureCurser = episodeCurser->FirstChildElement();
	}
}

void EpisodeConfRead::move_guide_feature_curser()
{
	guideFeatureCurser = guideFeatureCurser->NextSiblingElement();
	if(guideFeatureCurser == NULL)
	{
		cout<<"最后的引导特征，重新检索"<<endl;
		guideFeatureCurser = episodeCurser->FirstChildElement();
	}
	else
	{
		cout<<"下一特征"<<endl;
	}
}

void EpisodeConfRead::read_guide_curser()
{
	guideFeatureName = guideFeatureCurser->GetText();
	string featureImageName = guideFeatureName.append(".jpg");
	
	guideFeatureImage = cvLoadImage(featureImageName.data(), CV_BGRA2BGR);
	cout<<"guide feature名称:"<<featureImageName<<endl;
}

bool EpisodeConfRead::feature_ignore()
{
	TiXmlAttribute* featureFlag = guideFeatureCurser->FirstAttribute();
	string featureFlagName = featureFlag->Value();
	if(featureFlagName.compare("0") == 0)
	{
		return true;
	}
	return false;
}

void EpisodeConfRead::release_image()
{
	guideFeatureImage = NULL;
}