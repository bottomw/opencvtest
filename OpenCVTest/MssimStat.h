#pragma once
#include "stdafx.h"

using namespace std;
using namespace cv;

class MssimStat
{
public:
	queue<Mat> imageQueue;
	static MssimStat *get_instance();
	void queue_image(IplImage *img);
	bool scene_tranfer(void);

	static bool compareFlag;
	void help();
	~MssimStat(void);

private:
	MssimStat(void);
	static MssimStat *shareMssimStat;
	double getPSNR();
	CvScalar getMSSIM();
};

