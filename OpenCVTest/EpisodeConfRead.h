#pragma once
#include "stdafx.h"

using namespace std;

class EpisodeConfRead
{
public:
	
	static EpisodeConfRead *get_instance();
	~EpisodeConfRead(void);

	int episodeCurserFlag;
	void init(string &confFileName);

	TiXmlElement* rootElement;
	TiXmlElement* episodeCurser;
	TiXmlElement* episodeFeatureID;
	TiXmlElement* imageOffsetX;
	TiXmlElement* imageOffsetY;
	TiXmlElement* optionOffsetX;
	TiXmlElement* optionOffsetY;

	TiXmlAttribute* episodeType;
	TiXmlAttribute* episodeFlag;

	TiXmlElement* guideFeatureCurser;

	void move_episode_curser();
	void move_guide_feature_curser();
	bool curser_end();
	void read_xml_curser();
	void read_guide_curser();
	bool episode_type(string episodeType);
	bool episode_ignore();
	bool feature_ignore();

	string guideFeatureName;
	IplImage *guideFeatureImage;

	void release_image();
private:
	EpisodeConfRead(void);
	static EpisodeConfRead *shareEpisodeConfRead;
};