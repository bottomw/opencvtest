﻿#include "stdafx.h"
#include "CounterMark.h"

using namespace std;
using namespace cv;

CounterMark::CounterMark(void)
{

}

CounterMark::~CounterMark(void)
{

}

CounterMark *CounterMark::shareCounterMark = nullptr;
CounterMark *CounterMark::get_instance()
{
	if(shareCounterMark == NULL)
	{
		shareCounterMark = new CounterMark();
		return shareCounterMark;
	}
}

CvSeq *CounterMark::find_contours(IplImage *srcRaw)
{
	IplImage *dsw;
	IplImage *dst;
	IplImage *src;
	src = cvCreateImage(cvGetSize(srcRaw), 8, 1);
	//cvCopy(srcRaw, src);
	cvCvtColor(srcRaw, src, CV_BGRA2GRAY);

	dsw = cvCreateImage(cvGetSize(src), 8, 1);
	dst = cvCreateImage(cvGetSize(src), 8, 3);

	cvThreshold(src, dsw, 200, 255, CV_THRESH_BINARY); //二值化变换

	//cvShowImage("contour1", dsw);
	//cvWaitKey();

	CvMemStorage *memStorage;
	memStorage = cvCreateMemStorage(0);

	CvSeq *firstCounter;
	cvFindContours(dsw, memStorage, &firstCounter);

	cvReleaseImage(&dsw);
	cvReleaseImage(&dst);
	cvReleaseImage(&src);
	//cvReleaseImage(&srcRaw);
	cvClearMemStorage(memStorage);

	return firstCounter;
	//A storage for various OpenCV dynamic data structures, such as CvSeq, CvSet etc
	//用来创建一个内存存储器，来统一管理各种动态对象的内存。
	//函数返回一个新创建的内存存储器指针。
	//参数block_size对应内存器中每个内存块的大小，为0时内存块默认大小为64k。
	//这里貌似是用来放轮廓数据的
	//Dynamically growing sequence.
}

CvSeq *CounterMark::find_max_contour(CvSeq *contours)
{
	CvSeq *maxContour;

	int maxFlag = 0;
	int flag = 0;

	double area = 0, tmpArea = 0;

	for(CvSeq *c = contours;c != NULL;c = c->h_next)
	{
		area = fabs(cvContourArea(c, CV_WHOLE_SEQ));
		//cout<<"find contour area: "<<area<<endl;
		if(area > tmpArea)
		{
			maxFlag = flag;
			tmpArea = area;
		}
		flag++;
	}

	int index = 0;
	for(CvSeq *c = contours;c != NULL;c = c->h_next)
	{
		if(index == maxFlag)
		{
			return c;
		}
		index++;
	}

	return false;
}

CvSeq *CounterMark::match_contours(IplImage *featureImage, IplImage *sourceImage)
{
	CvSeq *featureContour = find_contours(featureImage);
	CvSeq *sourceContour = find_contours(sourceImage);

	CvSeq *maxFeatureContour = find_max_contour(featureContour);
	//cout<<"contour count: "<<sourceContour->elem_size<<endl;
	for(CvSeq *c = sourceContour; c != NULL; c=c->h_next)
	{
		//CvPoint *featureContourArray = (CvPoint*)malloc( featureCnt*sizeof(CvPoint) );
		//CvPoint *sourceContourArray = (CvPoint*)malloc( sourceCnt*sizeof(CvPoint) );
		float matchScore = cvMatchShapes(c, maxFeatureContour, CV_CONTOURS_MATCH_I1);
		int sourceArea = fabs(cvContourArea(c, CV_WHOLE_SEQ));
		int featureArea = fabs(cvContourArea(maxFeatureContour, CV_WHOLE_SEQ));
		float AreaRate = (float)sourceArea/featureArea;
		//cout<<"相似度:"<<matchScore<<","<<"源面积："<<sourceArea<<","<<"特征面积："<<featureArea<<","<<"面积比："<<AreaRate<<endl;
		if(matchScore < 0.05 && AreaRate > 0.5 && AreaRate < 2)
		{
			cout<<"相似度:"<<matchScore<<","<<"源面积："<<sourceArea<<","<<"特征面积："<<featureArea<<","<<"面积比："<<AreaRate<<endl;
			return c;
		}
	}

	return false;
	////findContours(InputOutputArray image, OutputArrayOfArrays contours, OutputArray hierarchy, int mode, int method, Point offset=Point())
}

CvPoint *CounterMark::convert_contour(CvSeq *contour, CvPoint *minLoc)
{
	int tourLength = contour->total;
	CvPoint *points = (CvPoint *)malloc(sizeof(CvPoint) * tourLength);

	CvSeqReader reader;
	CvPoint pt = cvPoint(0, 0);
	cvStartReadSeq(contour, &reader);
	topY = 10000000000;
	
	for(int i = 0; i < tourLength; i++)
	{
		CV_READ_SEQ_ELEM(pt, reader);
		points[i] = pt;
		if(pt.y < topY)
		{
			minLoc->y = pt.y;
			minLoc->x = pt.x;
			topX = minLoc->x;
			topY = minLoc->y;
		}
	}
	
	return points;
}
