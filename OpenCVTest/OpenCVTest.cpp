// OpenCVTest.cpp : 定义控制台应用程序的入口点。
//
#include "stdafx.h"
#include "CounterMark.h"
#include "ImageCapture.h"
#include "EpisodeConfRead.h"
#include "ProcessManager.h"
//#define _AFXDLL


using namespace cv;
using namespace std;

static Mat g_img_src;
static Mat g_img_dst;
static Mat g_img_sub;
static bool g_is_rect_inited = false;
static Point g_rect_tl;
static string g_window_name = "image";

// TODO: 在此处引用程序需要的其他头文件

char* window_name = "anchor";

//void mouse_click(Point clickpoint)
//{
//	LPPOINT currentPosition = 0;
//	GetCursorPos(currentPosition);
//
//	::SetCursorPos(clickpoint.x,clickpoint.y);
//	mouse_event(MOUSEEVENTF_LEFTDOWN|MOUSEEVENTF_LEFTUP,0,0,0,0);
//	//::SetCursorPos(currentPosition->x,currentPosition->y);
//	::SetCursorPos(500,500);
//}



static void onMouse(int event, int x, int y, int, void*)
{
	if(CV_EVENT_LBUTTONDOWN == event)
	{
		g_is_rect_inited = true;
		g_rect_tl = Point(x, y);
	}
	else if(CV_EVENT_MOUSEMOVE == event && g_is_rect_inited)
	{
		g_img_src.copyTo(g_img_dst);
		rectangle(g_img_dst, g_rect_tl, Point(x, y), Scalar_<uchar>::all(200), 3, 8);
		imshow(g_window_name, g_img_dst);
	}
	else if(CV_EVENT_LBUTTONUP == event && g_rect_tl != Point(x, y))
	{
		g_img_src(Rect(g_rect_tl, Point(x, y))).copyTo(g_img_sub);
		imshow("sub image", g_img_sub);
	}
}


//static int str2int(string str)
//{
//	stringstream sstr;
//	int value;
//	sstr<<str;
//	sstr>>value;
//	return value;
//}


int main(int argc, char** argv)
{
	ProcessManager *processManager;
	processManager = ProcessManager::get_instance();
	processManager->init();
	processManager->main_process_promote();
} 

 