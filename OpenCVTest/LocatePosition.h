#pragma once
#include "stdafx.h"

using namespace cv;
using namespace std;

static const char* sub_window_name = "sub window";

class LocatePoistion
{
public:
	LocatePoistion(void);
	~LocatePoistion(void);
	IplImage *sourceImage;

	void source_image(IplImage *srcImage);
	void locate_image_light(CvPoint minLoc, CvPoint maxLoc);
	bool identify_frame_image(IplImage *identifyImage);
	void locate_image_subimage(IplImage *anchorImage, CvPoint *minLoc, CvPoint *maxLoc);

private:
	static const int subImageSize = 20;
};