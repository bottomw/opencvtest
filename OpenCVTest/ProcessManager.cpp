#include "stdafx.h"
#include "ProcessManager.h"


ProcessManager::ProcessManager(void)
{
}


ProcessManager::~ProcessManager(void)
{
	if(ProcessManager::shareProcessManager)
	{
		delete ProcessManager::shareProcessManager;
	}
}

ProcessManager *ProcessManager::shareProcessManager = nullptr;
ProcessManager *ProcessManager::get_instance()
{
	if(shareProcessManager == NULL)
	{
		shareProcessManager = new ProcessManager();
		return shareProcessManager;
	}
}

void ProcessManager::init()
{
	confileNameTmp = "EpisodeConf.xml";
	string confileName(confileNameTmp);
	episodeConf = EpisodeConfRead::get_instance();
	episodeConf->init(confileName);
}

void ProcessManager::main_process_promote()
{

	frameCnt = 0;
	this->imageCapture = ImageCapture::get_instance();
	for(; episodeConf->curser_end() == false; episodeConf->move_episode_curser())
	{	
		episodeConf->read_xml_curser();
		if(episodeConf->episode_type("normal"))
		{
			if(episodeConf->episode_ignore())
			{
				cout<<"流程跳过"<<endl;
				continue;
			}
			cout<<"single none repeat process"<<endl;
			this->sub_process_sinlge_nonrepeat();
		}
		else if(episodeConf->episode_type("guide"))
		{
			cout<<"guide process"<<endl;
			this->sub_process_guide();
		}
		else
		{
			cout<<"流程类型不符，退出"<<endl;
			break;
		}
	}

}

void ProcessManager::sub_process_sinlge_nonrepeat()
{
	ps = before;
	string episodeFeatureID = episodeConf->episodeFeatureID->GetText();
	string featureImageName = episodeFeatureID.append(".jpg");
	IplImage *featureImage = cvLoadImage(featureImageName.data(), CV_BGRA2BGR);
	cout<<"episode名称:"<<episodeFeatureID<<endl;

	CvPoint minLoc, maxLoc;

	imagePosition.x = str2int(episodeConf->imageOffsetX->GetText());
	imagePosition.y = str2int(episodeConf->imageOffsetY->GetText());

	offsetVal.x = str2int(episodeConf->optionOffsetX->GetText());
	offsetVal.y = str2int(episodeConf->optionOffsetY->GetText());

	cout<<"特征位置:"<<imagePosition.x<<','<<imagePosition.y<<endl;
	cout<<"选择点击:"<<offsetVal.x<<','<<offsetVal.y<<endl;

	while(ps != pass)
	{
		frameCnt++;
		int retryCnt = 0;
		this->main_window_get();
		LocatePoistion locatePosition;
		locatePosition.source_image(imageCapture->mainWindow);
		locatePosition.locate_image_subimage(featureImage, &minLoc, &maxLoc);
		switch(ps)
		{
			case before:
			{			
				cout<<"监测特征位置:"<<minLoc.x<<','<<minLoc.y<<'\n';
				float ratio = float(imageCapture->mainWindowSize.width)/defaultWindowWidth;
				if(minLoc.x == imagePosition.x && minLoc.y == imagePosition.y) //找到特征子图像
				{
					cout<<"特征位置一致,执行节点"<<endl;
					ps = current;

					clickPoint = basicPoint + offsetVal;
					mouse_click(clickPoint);
					break;
				}
				else
				{
					cout<<"特征位置不一致,等待节点"<<endl;
					continue;
				}
			}
			case current:
			{
				cout<<"监测特征位置:"<<minLoc.x<<','<<minLoc.y<<'\n';
				if(minLoc.x == imagePosition.x && minLoc.y == imagePosition.y)
				{
					cout<<"点击似无响应，重试"<<endl;
					ps = before;
					retryCnt++;
					if(retryCnt > 3)
					{
						cout<<"重试失败"<<endl;
						ps = block;
					}
					break;
				}
				else
				{
					cout<<"节点执行成功";
					ps = pass;
					break;
				};
			}
			case block:
			{
				cout<<"流程受阻！"<<endl;
				POINT currentPosition;
				while(true)
				{
					GetCursorPos(&currentPosition);
					cout<<"偏置:"<<currentPosition.x-basicPoint.x<<','<<currentPosition.y-basicPoint.y<<'\n';
					Sleep(intervalMS);
				}
				break;
			};
		}
		Sleep(intervalMS);
	}
}

void ProcessManager::sub_process_guide()
{
	ps = before;

	CvPoint minLoc, maxLoc;

	int retryCnt = 0;
	mssimStat = MssimStat::get_instance();
	this->counterMark = CounterMark::get_instance();

	while(true)
	{
		frameCnt++;
		//cout<<ps<<endl;
		episodeConf->read_guide_curser();
		if(episodeConf->feature_ignore())
		{
			episodeConf->move_guide_feature_curser();
		}

		if(ps == before)
		{
			this->main_window_get();
			mssimStat->queue_image(imageCapture->mainWindow);
			//cout<<imageCapture->mainWindow->nSize<<endl;;
			auto contourLock = this->counterMark->match_contours(episodeConf->guideFeatureImage, imageCapture->mainWindow);
			imageCapture->release_image();
			if(contourLock != false)
			{
				retryCnt = 0;
				CvPoint* points = this->counterMark->convert_contour(contourLock, &minLoc);
				cout<<"轮廓匹配坐标："<<counterMark->topX<<","<<counterMark->topY<<endl;
				cout<<"特征位置一致,执行节点"<<endl;

				clickPoint.x = basicPoint.x + counterMark->topX;
				clickPoint.y = basicPoint.y + counterMark->topY;
				mouse_click(clickPoint);
				ps = current;
				continue;
			}
			else
			{
				cout<<"找不到匹配轮廓，重试"<<retryCnt<<endl;
				Sleep(200);
				this->main_window_get();
				mssimStat->queue_image(imageCapture->mainWindow);
				bool scenseTransFlag = false;
				scenseTransFlag	= mssimStat->scene_tranfer();
				if(scenseTransFlag)
				{
					cout<<"流程进行中，等待"<<endl;
					clickPoint.x = basicPoint.x + defaultClickPointX;
					clickPoint.y = basicPoint.y + defaultClickPointY;
					mouse_click(clickPoint);
				}
				else if(retryCnt<3)
				{
					cout<<"流程受阻，重试"<<endl;
					ps = before;
					retryCnt++;
					clickPoint.x = basicPoint.x + defaultClickPointX;
					clickPoint.y = basicPoint.y + defaultClickPointY;
					mouse_click(clickPoint);

				}
				else
				{
					ps = block;

				}
				imageCapture->release_image();
			}
		}
		else if(ps == current)
		{
			this->main_window_get();
			mssimStat->queue_image(imageCapture->mainWindow);
			bool scenseTransFlag = false;
			scenseTransFlag	= mssimStat->scene_tranfer();

			if(scenseTransFlag)
			{
				ps = pass;
			}
			else
			{
				ps = block;
			}
			imageCapture->release_image();
			continue;
		}
		else if(ps == block)
		{
			this->main_window_get();
			mssimStat->queue_image(imageCapture->mainWindow);
			cout<<"切换到下一特征"<<endl;
			retryCnt = 0;
			episodeConf->release_image();
			episodeConf->move_guide_feature_curser();
			ps = before;
			imageCapture->release_image();
			continue;
		}
		else if(ps == pass)
		{
			ps = before;
			//continue;
		}
		else
		{
			cout<<"error"<<endl;
			return;
		}
		//imageCapture->release_image();
		Sleep(200);
	}
}

int ProcessManager::str2int(string str)
{
	stringstream sstr;
	int value;
	sstr<<str;
	sstr>>value;
	return value;
};

void ProcessManager::mouse_click(Point clickpoint)
{
	LPPOINT currentPosition = 0;
	GetCursorPos(currentPosition);

	::SetCursorPos(clickpoint.x,clickpoint.y);
	mouse_event(MOUSEEVENTF_LEFTDOWN|MOUSEEVENTF_LEFTUP,0,0,0,0);
	//::SetCursorPos(currentPosition->x,currentPosition->y);
	::SetCursorPos(500,500);
}

void ProcessManager::main_window_get()
{
	cout<<"第"<<frameCnt<<"帧"<<endl;

	imageCapture->get_anchor();
	//this->imageCapture->release_image();
	this->imageCapture->init_gdi();
	
	basicPoint = imageCapture->gdi_screen_capture();
	cout<<"主界面基点:"<<basicPoint<<'\n';
}

bool position_judge(int detectVal, int standardVal)
{
	if(detectVal <= standardVal + 20 && detectVal >= standardVal - 20)
	{
		return true;
	}
	else
	{
		return false;
	}
}
