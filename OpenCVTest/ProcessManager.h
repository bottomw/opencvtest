#pragma once
#include "stdafx.h"
#include "EpisodeConfRead.h"
#include "ImageCapture.h"
#include "CounterMark.h"
#include "MssimStat.h"

class ProcessManager
{
public:
	static ProcessManager *get_instance();
	~ProcessManager(void);

	EpisodeConfRead *episodeConf;
	ImageCapture *imageCapture;
	LocatePoistion locatePosition;
	CounterMark *counterMark;
	MssimStat* mssimStat;

	Point basicPoint, imagePosition, offsetVal, clickPoint;
	CvSize mainWindowSize;

	enum ProgressStatus {before, current, pass, block} ps;
	int frameCnt;
	const char* confileNameTmp;

	void init(); 
	void main_process_promote();
	void main_window_get();

private:
	ProcessManager(void);
	static ProcessManager *shareProcessManager;
	void sub_process_guide();
	void sub_process_sinlge_nonrepeat();
	void mouse_click(Point clickpoint);
	int str2int(string str);
	bool position_judge(int detectVal, int standardVal);
	//IplImage *guideFeatureImage;
};

