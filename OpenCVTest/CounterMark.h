#pragma once
#include "stdafx.h"

class CounterMark
{
public:
	static CounterMark *get_instance();
	~CounterMark();
	CvSeq *find_max_contour(CvSeq *contours);
	CvSeq *find_contours(IplImage *srcRaw);
	CvSeq *match_contours(IplImage *featureImage, IplImage *sourceImage);
	CvPoint *convert_contour(CvSeq *contour, CvPoint *minLoc);
	//std::vector<std::vector<Point>> firstCounter;
	int topY, topX;
	double t;
private:
	CounterMark();
	static CounterMark *shareCounterMark;
};