# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
这个项目的主要功能，是通过opencv识别屏幕上腾讯手游助手的一些视觉特征，模拟鼠标点击达到辅助玩手游的目的，目前功能有限，只能过一下龙珠激斗的新手流程
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
主要入口：OpenCVTest
主要结构：
1、剧本：EpisodeConf.xml，会使用一些特征图形标记识别每一“幕”和对应的点击位置
2、剧本读取类：EpisodeConfRead，主要是用来读剧本到内存
3、导演类：processManager，做主要的流程控制、反馈控制
4、拍摄类：ImageCapture，负责从显存里面的图片读写和内存回收
5、定位类：LocatePosision，专门搜索图片并定位
6、评估类：MssimStat，通过像素计算评估两个图形的相近程度并返回相似指数；CounterMark，通过轮廓线评估两个图形的相似程度
* Dependencies
编译一个opencv2的库，提供一些数据结构，接口什么的方便操作图形
tinyxml，“剧本”是用xml写的
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact